package com.supinfobis;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class Serialisation {

    public static void main(String[] args)
    {
        //Nous factorison gson, et centralison tout dans cette variable
        //Nous enregistrongs par ailleurs un désérialiseur personnalisé, ainsi dès que l'on voudra désérialiser un objet de type Animal,
        //ce désérialiseur sera appelé.
        Gson gson = new GsonBuilder().setPrettyPrinting().registerTypeAdapter(Animal.class,new DeserialiserPersonnalise()).create();

        //Nous créons une liste de String dans laquelle nous ajoutons chacun de nos objets sérialisés.
        ArrayList<String> objetsJson = new ArrayList<String>();
        objetsJson.add(gson.toJson(new Chien("Medor","Waff waff !")));
        objetsJson.add(gson.toJson(new Chat("Shampo", "Miaou miaou !")));

        //Nous affichons chacuns des objets contenus dans la liste
        System.out.println(" -----------------");
        System.out.println("Deserialisation des objets ==> :");
        System.out.println(" -----------------");

        for(String objet : objetsJson)
        {
            System.out.println("Serialisation d'origine ==>");
            System.out.println(objet);

            Object objectInconnu = gson.fromJson(objet,Animal.class);

            System.out.println("Valeur de l'objet deserialise ==> "+objectInconnu.getClass());
        }
    }
}

class DeserialiserPersonnalise implements JsonDeserializer<Animal>
{
    @Override
    public Animal deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        //Nous savons par avance que c'est un jsonObject qui nous sera donné en entré.
        JsonObject jsonObject = (JsonObject) json;

        //Nous récupérons la valeur du champ classeOrigine directement dans le json, elle contient par exemple la valeur com.supinfobis.chat ou com.supinfobis.chien
        String classeOrigine = jsonObject.get("classeOrigine").getAsString();


        try {
            //Nous utilison la reflexion pour convertir dynamiquement la chaine de caractère obtenue en objet de type Class.
            Class classe = Class.forName(classeOrigine);

            //Nous avons obtenu grâce à la reflexion, la classe de l'objet à désérialiser. On utilise cette classe en tant que paramètre
            //de désérialisation, comme pour une désérialisation classique.
            return context.deserialize(json,classe);

        } catch (ClassNotFoundException e) {
            //En cas d'erreur nous affichons la classe non retrouvée
            System.out.println("Classe non retrouvee: "+classeOrigine);
        }

        return null;
    }
}

class Animal{
    String surnom;
    String classeOrigine;

    public Animal(String surnom)
    {
        this.surnom = surnom;
        this.classeOrigine= this.getClass().getName();
    }
}

class Chien extends Animal{

    String aboiment;

    public Chien(String surnom, String aboiment) {
        super(surnom);
        this.aboiment = aboiment;
    }
}

class Chat extends Animal{

    String miaulement;

    public Chat(String surnom, String miaulement) {
        super(surnom);
        this.miaulement = miaulement;
    }
}