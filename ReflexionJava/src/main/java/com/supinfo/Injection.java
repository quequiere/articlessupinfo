package com.supinfo;

import java.lang.reflect.Field;

public class Injection {

    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Maison m = new Maison();
        System.out.println(m.adresse);

        //Au lieu de retourner tous les fields, nous prenons uniquement celui qui nous interesse, 'nombreEtage'
        Field fieldNombreEtage = Maison.class.getDeclaredField("nombreEtage");

        //Nous forcons le champs à passer de private à public
        fieldNombreEtage.setAccessible(true);

        fieldNombreEtage.set(m,8);

        //Nous récupérons la valeur du champ (voir détail plus bas)
        Object object = fieldNombreEtage.get(m);

        //Nous affichons la valeur du champ
        System.out.println(object);
    }
}

class Maison{
    private int nombreEtage = 2;
    public String adresse = "France";

    public void setEtage(int etage)
    {
        if(etage>2)
            this.nombreEtage = 2;
        else
            this.nombreEtage = etage;
    }
}
