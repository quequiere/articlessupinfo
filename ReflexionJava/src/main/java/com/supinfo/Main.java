package com.supinfo;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath.ClassInfo;
import com.google.common.reflect.ClassPath;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {

        //Nous récupérons le classLoader qui contient l'ensemble des classes chargées par notre instance Java
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        //C'est ici que Guava nous simplifie la vie, il récupère toutes les classes qui sont dans le package com.supinfo, c'est une étape de la reflexion
        ImmutableSet<ClassInfo> classes = ClassPath.from(classLoader).getTopLevelClasses("com.supinfo");

        //Nous bouclons sur chaque metadata des classes trouvées (Main, Animal, Chien, Chat et Licorne
        for(ClassInfo classInfo : classes)
        {
            //Ici nous récupéron la réelle class à partir de son nom donnée par le ClassInfo, c'est encore une étape utilisant la reflexion
            Class targetClass = Class.forName(classInfo.getName());

            //Puis nous vérifions que la classe obtenue puisse être instancier depuis la classe Animal et que cette dernier ne soit pas elle même une interface
            if(Animal.class.isAssignableFrom(targetClass) && !targetClass.isInterface())
            {
                //Une autre force de la reflexion est de pouvoir instancier dynamiquement une classe sans avoir à préciser son type.
                Object monInstance = targetClass.newInstance();

                //Nous pouvons caster cette nouvelle instance en Animal puisque la classe est intansiable depuis animal, ceci a été vérifié dans le if ci dessus.
                Animal monAnimal = (Animal) monInstance;

                //Pour terminer nous pouvons enfin appeler notre fonction crier
                monAnimal.crier();
            }
        }
    }
}

interface Animal{

    void crier();
}

class Chien implements Animal{

    @Override
    public void crier() {
        System.out.println("Waf waf");
    }
}

class Chat implements Animal{
    @Override
    public void crier() {
        System.out.println("Miaou Miamou");
    }
}

class Licorne implements Animal{
    @Override
    public void crier() {
        System.out.println("Rainbowwww");
    }
}